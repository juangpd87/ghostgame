
Ghost Game

Ghost is a written word game in which players take turns to add letters to a english word (from left to right). 
The first player that completes a valid word longer than 3 characters or introduce a letter that make the current fragment not extendable as a valid word lost. 
 
Write a web app that play intelligently agains an human user using the english dictionary provided. Let the human play always first. 

The exercise will be evaluated considering the strategy used by the computer to win the game, clarity and legibility of code, 
architecture of the solution, usage of industry standard frameworks and test coverage. 

To accomplish this task we will use several criteria as quality code, readability, build system, using well known frameworks, testing practices, efficiency of the algorithms, etc

https://en.wikipedia.org/wiki/Ghost_(game)