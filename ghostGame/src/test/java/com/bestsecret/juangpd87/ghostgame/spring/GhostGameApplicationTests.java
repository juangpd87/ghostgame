package com.bestsecret.juangpd87.ghostgame.spring;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.bestsecret.juangpd87.ghostgame.spring.GhostGameApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = GhostGameApplication.class)
public class GhostGameApplicationTests {

    @Test
    public void contextLoads() {
    	GhostGameApplication.main(new String[] {});
    }

}

