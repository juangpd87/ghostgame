package com.bestsecret.juangpd87.ghostgame.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.bestsecret.juangpd87.ghostgame.exceptions.GameOverException;
import com.bestsecret.juangpd87.ghostgame.model.enums.FragmentType;
import com.bestsecret.juangpd87.ghostgame.model.enums.PlayerType;

public class GhostGameServiceImplTest {

	@Mock
	private DictionaryService dictionary;
	@Mock
	private IAService iaService;

	private GhostGameService ghostGameService;

	private static final Integer MAX_RANDOMS = 5;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		// Mocking checkString functionality
		when(dictionary.checkString(Mockito.anyString())).thenReturn(FragmentType.FRAGMENT);

		ghostGameService = new GhostGameServiceImpl(dictionary, iaService);
	}

	@Test
	public void testGhostGameServiceImpl() {
		assertNotNull(ghostGameService);
	}

	@Test
	public void testPlayHUMAN() {
		String fragmentPlayed = RandomStringUtils.randomAlphabetic(MAX_RANDOMS);

		// A player´s turn
		String stringObtained = ghostGameService.play(PlayerType.HUMAN.getId(), fragmentPlayed);
		assertEquals(fragmentPlayed, stringObtained);
	}

	@Test
	public void testPlayIA() {
		String fragmentPlayed = RandomStringUtils.randomAlphabetic(MAX_RANDOMS);
		when(iaService.thinkBestOption(Mockito.anyString())).thenReturn(fragmentPlayed);
		// A player´s turn
		String stringObtained = ghostGameService.play(PlayerType.IA.getId(), fragmentPlayed);
		assertEquals(fragmentPlayed, stringObtained);
	}

	@Test
	public void testNotAWord() {
		String fragmentPlayed = RandomStringUtils.randomAlphabetic(MAX_RANDOMS);
		Integer player = Integer.valueOf(RandomStringUtils.randomNumeric(MAX_RANDOMS));
		// Mocking checkString functionality
		when(dictionary.checkString(Mockito.anyString())).thenReturn(FragmentType.NOT_A_WORD);
		// A player´s turn
		try {
			String stringObtained = ghostGameService.play(player, fragmentPlayed);
			fail("GameOverException: NOT A WORD not throws.");
		} catch (GameOverException e) {
			assertEquals(FragmentType.NOT_A_WORD, e.getFragmentType());
			assertEquals(player, e.getIdPlayer());

		}
	}

	@Test
	public void testAWord() {
		String fragmentPlayed = RandomStringUtils.randomAlphabetic(MAX_RANDOMS);
		Integer player = Integer.valueOf(RandomStringUtils.randomNumeric(MAX_RANDOMS));
		// Mocking checkString functionality
		when(dictionary.checkString(Mockito.anyString())).thenReturn(FragmentType.WORD);
		// A player´s turn
		try {
			String stringObtained = ghostGameService.play(player, fragmentPlayed);
			fail("GameOverException: WORD not throws.");
		} catch (GameOverException e) {
			assertEquals(FragmentType.WORD, e.getFragmentType());
			assertEquals(player, e.getIdPlayer());

		}
	}

}
