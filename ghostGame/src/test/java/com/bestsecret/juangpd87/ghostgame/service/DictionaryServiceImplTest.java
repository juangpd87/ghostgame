package com.bestsecret.juangpd87.ghostgame.service;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.bestsecret.juangpd87.ghostgame.exceptions.DictionaryNotFoundException;
import com.bestsecret.juangpd87.ghostgame.model.enums.FragmentType;

public class DictionaryServiceImplTest {

	private final static String SERVER_DICTIONARY_PATH = "src/main/resources/files/gosthGameDict.txt";
	private final static Integer MIN_LENGTH_WORDS = 3;
	private final static Integer LIMIT_RANDOM = 5;
	private final static String WORD_OK = "aabac";
	private final static String FRAGMENT = "aaba";
	private final static String WORD_KO = "aabaca";

	private DictionaryServiceImpl dictionary;

	@Before
	public void setUp() throws Exception {
		dictionary = new DictionaryServiceImpl(SERVER_DICTIONARY_PATH, MIN_LENGTH_WORDS);
	}

	@Test
	public void constructors() throws Exception {
		dictionary = new DictionaryServiceImpl(SERVER_DICTIONARY_PATH, MIN_LENGTH_WORDS);
		assertNotNull(dictionary);

		try {
			dictionary = new DictionaryServiceImpl(randomAlphabetic(LIMIT_RANDOM), null);
			fail("DictionaryNotFoundException not throws.");
		} catch (DictionaryNotFoundException e) {
		}

	}

	@Test
	public void testCheckWord() {
		assertEquals(FragmentType.WORD, dictionary.checkString(WORD_OK));
		assertEquals(FragmentType.FRAGMENT, dictionary.checkString(FRAGMENT));
		assertEquals(FragmentType.NOT_A_WORD, dictionary.checkString(WORD_KO));
	}

	@Test
	public void testNextValidOptions() {
		List<Character> options = dictionary.nextValidOptions("aba");
		assertNotEquals(0, options.size());

		options = dictionary.nextValidOptions(WORD_KO);
		assertEquals(0, options.size());
	}

}
