package com.bestsecret.juangpd87.ghostgame.web;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.ui.Model;

import com.bestsecret.juangpd87.ghostgame.exceptions.GameOverException;
import com.bestsecret.juangpd87.ghostgame.model.enums.FragmentType;
import com.bestsecret.juangpd87.ghostgame.service.GhostGameService;
import com.bestsecret.juangpd87.ghostgame.web.model.AJAXNotification;

public class GhostGameControllerTest {

	@Mock
	private GhostGameService ghostGameService;
	@Mock
	private Model model;

	private GhostGameController controller;

	private static final Integer MAX_RANDOMS = 5;
	private static final String HTML_GHOST_GAME = "ghost_game";

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);

		controller = new GhostGameController(ghostGameService);
	}

	@Test
	public void testGhostGameController() {
		assertNotNull(controller);
	}

	@Test
	public void testStartMatch() {
		// Model equals null
		String strPath = controller.startMatch(null);
		assertEquals(HTML_GHOST_GAME, strPath);
		// Model mocked
		strPath = controller.startMatch(model);
		assertEquals(HTML_GHOST_GAME, strPath);
	}

	@Test
	public void testSetComboLetters() {
		char[] optionsLetters = controller.setComboLetters();
		assertNotEquals(0, optionsLetters.length);
	}

	@Test
	public void testPlay() {
		// Mocked an OK fragment
		when(ghostGameService.play(Mockito.anyInt(), Mockito.anyString()))
				.thenReturn(RandomStringUtils.randomAlphabetic(MAX_RANDOMS));
		AJAXNotification notification = controller.play(null, null, null);
		assertNotNull(notification);
		assertTrue(notification.getFollowTheGame());

		// Mocked an KO fragment
		Integer idPlayer = Integer.parseInt(RandomStringUtils.randomNumeric(MAX_RANDOMS));
		String fragment = RandomStringUtils.randomAlphabetic(MAX_RANDOMS);

		when(ghostGameService.play(Mockito.anyInt(), Mockito.anyString()))
				.thenThrow(new GameOverException(idPlayer, FragmentType.NOT_A_WORD));

		notification = controller.play(model, idPlayer, fragment);

		assertNotNull(notification);
		assertEquals(idPlayer, notification.getIdPlayer());
		assertEquals(fragment, notification.getFragment());
		assertEquals(FragmentType.NOT_A_WORD.getDescription(), notification.getDescription());
		assertFalse(notification.getFollowTheGame());

	}

}
