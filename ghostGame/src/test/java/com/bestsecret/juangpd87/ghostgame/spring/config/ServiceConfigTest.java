package com.bestsecret.juangpd87.ghostgame.spring.config;

import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;

import com.bestsecret.juangpd87.ghostgame.exceptions.DictionaryNotFoundException;
import com.bestsecret.juangpd87.ghostgame.service.DictionaryService;
import com.bestsecret.juangpd87.ghostgame.service.GhostGameService;
import com.bestsecret.juangpd87.ghostgame.service.IAService;

@PropertySource("classpath:config/gameConfiguration.properties")
public class ServiceConfigTest {

	@Autowired
	private ServiceConfig serviceConfig;

	@Mock
	DictionaryService dictionaryMocked;
	@Mock
	IAService iaMocked;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);

		serviceConfig = new ServiceConfig();
	}

	@Test
	public void testDictionaryService() {

		DictionaryService dict = null;
		try {
			dict = serviceConfig.dictionaryService();
		} catch (DictionaryNotFoundException e) {
			
		}
	}

	@Test
	public void testIaService() {
		IAService iaService = serviceConfig.iaService(null);
		assertNotNull(iaService);

		iaService = serviceConfig.iaService(dictionaryMocked);
		assertNotNull(iaService);
	}

	@Test
	public void testGhostGameService() {
		GhostGameService ghostGameService = serviceConfig.ghostGameService(null, null);
		assertNotNull(ghostGameService);

		ghostGameService = serviceConfig.ghostGameService(dictionaryMocked, iaMocked);
		assertNotNull(ghostGameService);
	}

}
