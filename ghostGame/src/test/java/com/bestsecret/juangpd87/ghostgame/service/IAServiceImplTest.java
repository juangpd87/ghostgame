package com.bestsecret.juangpd87.ghostgame.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class IAServiceImplTest {

	@Mock
	private DictionaryService dictionary;

	private IAService iaService;

	private static final Integer MAX_RANDOMS = 5;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		// Mocking checkString functionality
		when(dictionary.nextValidOptions(Mockito.anyString())).thenReturn(getRandomOptions());

		iaService = new IAServiceImpl(dictionary);
	}

	@Test
	public void testIAServiceImpl() {
		assertNotNull(iaService);
	}

	@Test
	public void testThinkBestOption() {
		String fragmentPlayed = RandomStringUtils.randomAlphabetic(MAX_RANDOMS);
		String strObtained = iaService.thinkBestOption(fragmentPlayed);
		assertEquals(fragmentPlayed, strObtained.substring(0, strObtained.length() - 1));
	}

	private List<Character> getRandomOptions() {
		List<Character> options = new LinkedList<Character>();
		options.add(new Character(RandomStringUtils.randomAlphabetic(1).charAt(0)));
		options.add(new Character(RandomStringUtils.randomAlphabetic(1).charAt(0)));
		return options;
	}

}
