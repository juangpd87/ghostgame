/* Funcionalidad para la navegación en la aplicación */
var baseURL = window.location.protocol + "//" + window.location.host;
var playURL = "/play";
var idHumanPlayer = 0;
var idIAPlayer = 1;

$(document).ready(gameEvents());

/** All the game events are declared here */
function gameEvents() {

	// Change events on letter SELECT combo
	$("#comboLetters").change(function(e) {
		var fragment = $("#fragment").val() + $("#comboLetters").val();
		doThePlay(idHumanPlayer, fragment);
	});

	// At first time, labelIA must be hide
	$("#labelIA").hide();
}

/** do the player´s play */
function doThePlay(idPlayer, fragment) {
	// AJAX call
	var urlAJAX = baseURL + playURL + "?idPlayer=" + idPlayer + "&fragment="
			+ fragment;

	// Llamada AJAX para la actualización del elemento
	$.ajax({
		type : "GET",
		url : urlAJAX,
		data : {},
		cache : false,
		success : function(notification) {
			$("#labelIA").hide();
			if (!notification.followTheGame) {
				alert(getPlayerName(idPlayer) + " loses\n "
						+ notification.description + " :\n\n  "
						+ notification.fragment);
				$("#fragment").val("");
				$("#comboLetters").val("");
			} else {
				$("#fragment").val(notification.fragment);
				if (idPlayer == idHumanPlayer) {
					doTheIAPlay(notification.fragment);
				}

			}

		},
		error : function(notification) {// Notificación en caso de error
			alert(notification.description);

		}
	});
}

/** IA´s turn */
function doTheIAPlay(fragment) {
	// Show text "IA is playing ... "
	$("#labelIA").show();
	setTimeout(function() {

		doThePlay(idIAPlayer, fragment);
		// Disable select
		$("#comboLetters").val("");
	}, 2000);

}

/** Return the player name */
function getPlayerName(idPlayer) {
	if (idPlayer == idHumanPlayer) {
		return "Player";
	} else if (idPlayer == idIAPlayer) {
		return "IA";
	}

}
