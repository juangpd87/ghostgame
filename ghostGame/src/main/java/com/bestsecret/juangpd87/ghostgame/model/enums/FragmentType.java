package com.bestsecret.juangpd87.ghostgame.model.enums;

import lombok.Getter;

@Getter
public enum FragmentType {
	NOT_A_WORD(0, "The fragment can not be a valid word"), 
	FRAGMENT(1, "Valid fragment"), 
	WORD(2,"The fragment is a word found in the dictionary");

	private Integer option;
	private String description;

	private FragmentType(Integer option, String description) {
		this.option = option;
		this.description = description;
	}

}
