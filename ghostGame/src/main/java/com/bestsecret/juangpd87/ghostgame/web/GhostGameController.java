package com.bestsecret.juangpd87.ghostgame.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bestsecret.juangpd87.ghostgame.exceptions.GameOverException;
import com.bestsecret.juangpd87.ghostgame.service.GhostGameService;
import com.bestsecret.juangpd87.ghostgame.web.model.AJAXNotification;

@Controller
public class GhostGameController {

	private static Logger log = LoggerFactory.getLogger(GhostGameController.class);

	private static final String HTML_GHOST_GAME = "ghost_game";
	private static final String ALL_THE_LETTERS = " abcdefghijklmnopqrstuvwxyz";

	private GhostGameService ghostGameService;

	public GhostGameController(final GhostGameService ghostGameService) {
		super();
		this.ghostGameService = ghostGameService;
	}

	@GetMapping({"/ghostGame","/"})
	public String startMatch(Model model) {
		log.error("startMatch");
		return HTML_GHOST_GAME;
	}

	/** it searchs this id (comboLetters) and add this returns to that component */
	@ModelAttribute("comboLetters")
	public char[] setComboLetters() {
		// Fill the combo with all the letters
		return ALL_THE_LETTERS.toCharArray();
	}

	/**
	 * AJAX response to every play of every player
	 */
	@RequestMapping(value = "/play", method = { RequestMethod.POST, RequestMethod.GET })
	public @ResponseBody AJAXNotification play(final Model model, final Integer idPlayer, final String fragment) {
		log.info("play - Player:{} - fragment:{}", idPlayer, fragment);
		AJAXNotification notification = new AJAXNotification(true, fragment, "", idPlayer);
		try {
			notification.setFragment(ghostGameService.play(idPlayer, fragment));
		} catch (GameOverException goe) {
			log.error("play - GameOverException: {}", goe.getFragmentType().getDescription());
			notification.setFollowTheGame(false);
			notification.setDescription(goe.getFragmentType().getDescription());
		}
		// Call the service to check this play
		return notification;
	}

}
