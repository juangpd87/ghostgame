package com.bestsecret.juangpd87.ghostgame.exceptions;

import com.bestsecret.juangpd87.ghostgame.model.enums.FragmentType;

import lombok.Data;

@Data
public class GameOverException extends RuntimeException {

	private Integer idPlayer;
	private FragmentType fragmentType;

	public GameOverException(final Integer idPlayer, final FragmentType fragmentType) {
		super();
		this.idPlayer = idPlayer;
		this.fragmentType = fragmentType;
	}

}
