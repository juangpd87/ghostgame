package com.bestsecret.juangpd87.ghostgame.web.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AJAXNotification {

	private Boolean followTheGame;
	private String fragment;
	private String description;
	private Integer idPlayer;

}
