package com.bestsecret.juangpd87.ghostgame.service;

import com.bestsecret.juangpd87.ghostgame.exceptions.GameOverException;
import com.bestsecret.juangpd87.ghostgame.model.enums.FragmentType;
import com.bestsecret.juangpd87.ghostgame.model.enums.PlayerType;

public class GhostGameServiceImpl implements GhostGameService {

	private DictionaryService dictionary;
	private IAService ia;

	public GhostGameServiceImpl(final DictionaryService dictionary, final IAService ia) {
		super();
		this.dictionary = dictionary;
		this.ia = ia;
	}

	@Override
	public String play(final Integer idPlayer, final String fragment) {
		FragmentType resolution;
		String fragmentPlayed = fragment;

		if (PlayerType.IA.getId() == idPlayer) {
			// IA´s turn
			fragmentPlayed = ia.thinkBestOption(fragment);
		}

		// Check the fragment
		resolution = dictionary.checkString(fragmentPlayed);

		switch (resolution) {
			case NOT_A_WORD: {
				// Fragment is not going to be a word, player loses
				throw new GameOverException(idPlayer, resolution);
			}
			case WORD: {
				// Fragment is a word, player loses
				throw new GameOverException(idPlayer, resolution);
			}
			case FRAGMENT: {
				// Fragment could be a word, the game continue
				break;
			}
		}

		return fragmentPlayed;
	}

}
