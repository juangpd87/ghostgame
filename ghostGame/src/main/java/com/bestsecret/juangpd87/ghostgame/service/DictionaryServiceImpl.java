package com.bestsecret.juangpd87.ghostgame.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bestsecret.juangpd87.ghostgame.exceptions.DictionaryNotFoundException;
import com.bestsecret.juangpd87.ghostgame.model.TreeNode;
import com.bestsecret.juangpd87.ghostgame.model.enums.FragmentType;

public class DictionaryServiceImpl implements DictionaryService {

	private static Logger log = LoggerFactory.getLogger(DictionaryServiceImpl.class);

	private HashMap<Character, TreeNode> dictionary;

	public DictionaryServiceImpl(final String dictionaryPath, final Integer minLengthWords)
			throws DictionaryNotFoundException {
		super();
		dictionary = new HashMap<Character, TreeNode>();
		loadDictionary(dictionaryPath, minLengthWords);
	}

	/**
	 * Reads the file passed by argument and create an structure to save all the
	 * words wich lengths is above minLengthWords given
	 * 
	 * @throws DictionaryNotFoundException
	 */
	private void loadDictionary(final String dictionaryPath, final Integer minLengthWords)
			throws DictionaryNotFoundException {
		log.info("loadDictionary: dictionaryPath: {}", dictionaryPath);

		if (dictionaryPath == null) {
			throw new DictionaryNotFoundException("Dictionary path is null");
		}
		Path path = Paths.get(dictionaryPath);
		try (Stream<String> stream = Files.lines(path)) {
			stream.filter(word -> word.length() >= minLengthWords).forEach(word -> {
				String normWord = normalizeWord(word);
				saveWord(normWord, dictionary);
			});

		} catch (IOException e) {
			log.error("loadDictionary: IOException - {} ", e.getMessage());
			throw new DictionaryNotFoundException(e.getMessage());
		}

		log.info("loadDictionary: end loadDictionary");
	}

	/** Remove all the stranger characters appeared */
	private String normalizeWord(final String word) {
		String normWord = word;
		normWord = normWord.replaceAll("'", "");
		normWord = normWord.replaceAll("/", "");
		return normWord;
	}

	/**
	 * Recursive functionality to store a word in the tree
	 */
	private void saveWord(final String word, final HashMap<Character, TreeNode> node) {

		Optional<TreeNode> optNode = Optional.ofNullable(node.get(word.charAt(0)));

		if (optNode.isPresent()) {
			if (word.length() > 1) {
				// Check if the rest of the word is stored
				saveWord(word.substring(1), optNode.get().getSons());
			} else {
				// Mark this node as the end of the word
				optNode.get().setEndOfAWord(true);
			}

		} else {
			// This char is not stored, it must be stored (and also the rest of the word)
			TreeNode newNode = new TreeNode(word.charAt(0), new HashMap<Character, TreeNode>());
			if (word.length() == 1) {
				// Mark this node as the end of the word
				newNode.setEndOfAWord(true);
			}
			node.put(word.charAt(0), newNode);

			if (word.length() > 1) {
				// Save the rest of the word
				saveWord(word.substring(1), newNode.getSons());
			} else {
				// Mark this node as the end of the word
				newNode.setEndOfAWord(true);
			}

		}
	}

	/**
	 * Check existence of word in dictionary
	 */
	public FragmentType checkString(final String fragment) {
		return checkWord(fragment, dictionary);
	}

	private FragmentType checkWord(final String word, final HashMap<Character, TreeNode> node) {
		FragmentType existence;

		Optional<TreeNode> optNode = Optional.ofNullable(node.get(word.charAt(0)));
		if (optNode.isPresent()) {
			if (word.length() > 1) {
				// Check if the rest of the word is stored
				existence = checkWord(word.substring(1), optNode.get().getSons());
			} else if (optNode.get().getEndOfAWord()) {
				// The word is inside the dictionary
				existence = FragmentType.WORD;
			} else {
				// The fragment is inside the dictionary, but it´s not a word
				existence = FragmentType.FRAGMENT;
			}
		} else {
			existence = FragmentType.NOT_A_WORD;
		}

		return existence;
	}

	@Override
	public List<Character> nextValidOptions(final String fragment) {
		return Arrays.asList(nextValidOptions(fragment, dictionary));
	}

	private Character[] nextValidOptions(final String word, final HashMap<Character, TreeNode> node) {
		Character[] options = new Character[] {};

		Optional<TreeNode> optNode = Optional.ofNullable(node.get(word.charAt(0)));
		if (optNode.isPresent()) {
			if (word.length() > 1) {
				// Check if the rest of the word is stored
				options = nextValidOptions(word.substring(1), optNode.get().getSons());
			} else {
				// The word is inside the dictionary
				options = getAllOptions(optNode.get());
			}
		}

		return options;
	}

	/** Return all the characters to do a word */
	private Character[] getAllOptions(final TreeNode node) {
		return node.getSons().keySet().toArray(new Character[] {});
	}

}
