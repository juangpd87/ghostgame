package com.bestsecret.juangpd87.ghostgame.service;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IAServiceImpl implements IAService {

	private static Logger log = LoggerFactory.getLogger(IAServiceImpl.class);

	private DictionaryService dictionary;

	public IAServiceImpl(final DictionaryService dictionary) {
		super();
		this.dictionary = dictionary;
	}

	@Override
	public String thinkBestOption(final String fragment) {
		log.info("thinkBestOption: Initial fragment {}", fragment);

		List<Character> options = dictionary.nextValidOptions(fragment);
		log.info("thinkBestOption: options founded {}", options);

		// TODO optimize this selection
		String newFragment = fragment;
		if (options.size() > 0) {
			// For all the options, get the option with less childrens (to make it difficult
			// for the user to continue the word)
			Optional<Character> optMinChar = options.stream().min(new Comparator<Character>() {
				@Override
				public int compare(Character o1, Character o2) {
					List<Character> options1 = dictionary.nextValidOptions(fragment + o1);
					log.info("thinkBestOption: {} have {} options:{}", o1, options1.size(), options1);
					List<Character> options2 = dictionary.nextValidOptions(fragment + o2);
					log.info("thinkBestOption: {} have {} options:{}", o2, options2.size(), options2);
					return options1.size() - options2.size();
				}
			});
			if (optMinChar.isPresent()) {
				log.info("thinkBestOption: best option is {}", optMinChar.get());
				newFragment = fragment + optMinChar.get();
			} else {
				log.info("thinkBestOption: any option found: {}", optMinChar.get());
			}
		}
		return newFragment;
	}

}
