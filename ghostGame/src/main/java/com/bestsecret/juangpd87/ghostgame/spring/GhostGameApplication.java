package com.bestsecret.juangpd87.ghostgame.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan("com.bestsecret.juangpd87.ghostgame")
public class GhostGameApplication {

	public static void main(String[] args) {
		SpringApplication.run(GhostGameApplication.class, args);
	}

}
