package com.bestsecret.juangpd87.ghostgame.model.enums;

import lombok.Getter;

@Getter
public enum PlayerType {

	HUMAN(0), IA(1);
	private Integer id;

	PlayerType(final Integer id) {
		this.id = id;
	}

}
