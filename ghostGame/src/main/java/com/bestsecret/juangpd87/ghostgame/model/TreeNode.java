package com.bestsecret.juangpd87.ghostgame.model;

import java.util.HashMap;

import lombok.Data;

/**
 * Node of tree structure used to save the dictionary
 */
@Data
public class TreeNode {

	private Character key;
	private Boolean endOfAWord;
	private HashMap<Character, TreeNode> sons;

	public TreeNode(Character key, HashMap<Character, TreeNode> sons) {
		super();
		this.key = key;
		this.endOfAWord = false;
		this.sons = sons;
	}

}
