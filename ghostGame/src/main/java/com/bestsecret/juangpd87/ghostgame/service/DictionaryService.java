package com.bestsecret.juangpd87.ghostgame.service;

import java.util.List;

import com.bestsecret.juangpd87.ghostgame.model.enums.FragmentType;

public interface DictionaryService {
	
	public FragmentType checkString(final String fragment);

	public List<Character> nextValidOptions(final String fragment);
}
