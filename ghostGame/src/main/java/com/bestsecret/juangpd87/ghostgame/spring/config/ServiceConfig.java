package com.bestsecret.juangpd87.ghostgame.spring.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import com.bestsecret.juangpd87.ghostgame.exceptions.DictionaryNotFoundException;
import com.bestsecret.juangpd87.ghostgame.service.DictionaryService;
import com.bestsecret.juangpd87.ghostgame.service.DictionaryServiceImpl;
import com.bestsecret.juangpd87.ghostgame.service.GhostGameService;
import com.bestsecret.juangpd87.ghostgame.service.GhostGameServiceImpl;
import com.bestsecret.juangpd87.ghostgame.service.IAService;
import com.bestsecret.juangpd87.ghostgame.service.IAServiceImpl;

/**
 * Here the creations of the Service beans are defined
 */

@Configuration
@PropertySource("classpath:config/gameConfiguration.properties")
public class ServiceConfig {

	@Value("${dictionary.minLengthWord}")
	private Integer minLengthWord;

	@Value("${dictionary.path}")
	private String path;

	@Bean
	public DictionaryService dictionaryService() throws DictionaryNotFoundException {
		return new DictionaryServiceImpl(path, minLengthWord);
	}

	@Bean
	public IAService iaService(final DictionaryService dictionaryService) {
		return new IAServiceImpl(dictionaryService);
	}

	@Bean
	public GhostGameService ghostGameService(final DictionaryService dictionaryService, final IAService iaService) {
		return new GhostGameServiceImpl(dictionaryService, iaService);
	}

}
