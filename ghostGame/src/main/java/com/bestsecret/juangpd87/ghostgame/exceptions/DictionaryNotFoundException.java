package com.bestsecret.juangpd87.ghostgame.exceptions;

public class DictionaryNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DictionaryNotFoundException(final String message) {
		super(message);
	}

}
