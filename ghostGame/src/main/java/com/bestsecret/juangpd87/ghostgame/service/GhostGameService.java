package com.bestsecret.juangpd87.ghostgame.service;

public interface GhostGameService {

	String play(final Integer idPlayer, final String fragment);

}
